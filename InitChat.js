import React from 'react';
import Chat from '../../components/Chat';
import { CHAT, BOTTOM_TAB } from '../../navigator/RouteName';
import { CommonActions } from '@react-navigation/native';

export default class InitChat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    render() {
        console.log('userId:' + this.props?.route?.params?.userId)
        return (
            <>
                <Chat userId={this.props?.route?.params?.userId} navigation={this.props.navigation} name={this.props?.route?.params?.name} extraData={this.props.route?.params?.extraData}></Chat>
            </>
        )
    }
}