import React from 'react';
import {
    Text, View, TextInput, StyleSheet, ScrollView, Image, TouchableOpacity, AsyncStorage, Keyboard, Platform
} from 'react-native';
import Header from '../../components/Header';
import { scale, scaleModerate, scaleVertical } from '../../utils/Scale';
import { isLargePhone } from '../../constant/Layout';
import { GRAY_FONTCOLOR, WHITE, PRIMARY_COLOR, GREEN, RED, BACKGROUND_COLOR, GRAY_LIGHT, BLACK, BLUE } from '../../constant/Colors';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { texts } from '../../constant/CommonStyles';
import { callApiWithParams } from '../../api/CustomAPI';
import { INITCHAT, LOGIN } from '../../navigator/RouteName';
import { GET_LIST_CHAT_USER, MEDIA_URL } from '../../constant/EndPoint';
import { Toast } from 'native-base';
import { CommonActions } from '@react-navigation/native';
import Loading from '../../components/Loading';
import { SOCKET_DEBUG, socketInstance } from '../../constant/SocketConfig';
import { windowWidth, windowHeight, headerHeight } from '../../constant/Layout';
import moment from 'moment';
import ImageIcon from '../../components/ImageIcon';
import { connect } from 'react-redux';
import { requestUpdateContactScreen } from '../../common_actions/SystemAction';
import { GET_LIST_CONTACT, SEARCH_CHAT } from '../../common_constant/EndPoint';
import FastImage from 'react-native-fast-image';
import { getString } from '../../utils/GetString';
class ChatScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            isLoading: false,
            dataSearch: '',
            dataUserSearched: [],
            callingSearchAPI: false,
            screenDisplay: 'chat',
            listContacts: {
                admin: [],
                currentMonitor: [],
                monitors: [],
                parentByClass: [],
                parentToday: [],
                teacherByStudent: [],
                teacherToday: []
            }
        }
    }

    componentDidMount() {
        this.initializeSocket()
        this._getChatUsers()
        this._getContactsList()
        this.props.requestUpdateContactScreen(false)
    }
    async _getContactsList() {
        const contactResponse = await callApiWithParams('GET', GET_LIST_CONTACT)
        if (contactResponse && contactResponse.status === 200 && contactResponse.data?.code === 0) {
            this.setState({
                ...this.state,
                listContacts: contactResponse?.data?.data
            })
        }
    }

    initializeSocket() {
        socketInstance.getInstance().on('connect', async () => {
            console.log('connected')
            const token = await AsyncStorage.getItem('token')
            let dataClient = {
                token: token
            }
            console.log('token:', token)
            socketInstance.getInstance().emit('authenticate', JSON.stringify(dataClient))
        });
        socketInstance.getInstance().on('disconnect', function () {
            console.log('disconnected')
        });
        socketInstance.getInstance().on('unread-server-message', (messages) => {
            const messageParse = JSON.parse(messages)
            let newestUserMessage = {
                active: true,
                created_at: messageParse?.createAt,
                from_me: false,
                id: messageParse?.sendId,
                image_path: messageParse?.user?.avatar,
                last_message: messageParse?.text,
                name: messageParse?.user?.name,
                read: false,
                user_type_id: messageParse?.user_type_id || 1
            }

            let history = this.state.users
            for (let i = 0; i < history.length; i++) {
                if (history[i]?.id === newestUserMessage?.id) {
                    history.splice(i, 1);
                    break;
                }
            }
            history = [
                newestUserMessage,
                ...history
            ]
            this.setState({
                users: history,
            })
        });
        socketInstance.getInstance().on('server-online', (user) => {
            let isFoundUser = false
            let history = this.state.users
            for (let i = 0; i < history.length; i++) {
                if (history[i]?.id === user?.id) {
                    isFoundUser = true;
                    history[i] = {
                        ...history[i],
                        active: true
                    }
                    break;
                }
            }
            this.setState({
                ...this.state,
                users: history,
            })

        });
        socketInstance.getInstance().on('server-offline', (user) => {
            let isFoundUser = false
            let history = this.state.users
            for (let i = 0; i < history.length; i++) {
                if (history[i]?.id === user?.id) {
                    isFoundUser = true;
                    history[i] = {
                        ...history[i],
                        active: false
                    }
                    break;
                }
            }
            this.setState({
                ...this.state,
                users: history,
            })
        });
    }
    async _getChatUsers() {
        this.setState({
            ...this.state,
            isLoading: true
        })
        const response = await callApiWithParams('GET', GET_LIST_CHAT_USER);
        this.setState({
            ...this.state,
            isLoading: false
        })

        if (response) {
            if (response.status === 200) {
                if (response.data?.code === 0) {
                    let dataHistory = response.data?.data?.history
                    for (let i = 0; i < dataHistory.length; i++) {
                        if (dataHistory[i]?.last_message === '') {
                            dataHistory[i] = {
                                ...dataHistory[i],
                                last_message: getString('COMMON_MESS_SCREEN_SEND_A_IMAGE')
                            }
                        }
                    }
                    this.setState({
                        users: dataHistory,
                    })
                }
            } else if (response.status === 401) {
                this._tokenInvalidFunction()
            }
        } else {

        }
    }
    async _tokenInvalidFunction() {
        Toast.show({
            text: 'Phiên đăng nhập đã hết hạn, bạn sẽ được quay trở về trang đăng nhập.',
            position: "bottom",
            duration: 3000
        })
        await AsyncStorage.removeItem('token');
        this.props.navigation.dispatch(
            CommonActions.reset({
                index: 1,
                routes: [{ name: LOGIN }],
            }),
        );
    }

    _renderUsers(item, index, length) {
        console.log('user chat:', item)
        let isFirstItem = false
        let isLastItem = false

        if (index !== undefined) {
            if (index === 0) {
                isFirstItem = true
            }
        }
        if (index === length) {
            isLastItem = true
        }
        return (
            <TouchableOpacity
                onPress={() => this.props.navigation.navigate(INITCHAT, {
                    userId: item.id,
                    name: item.name,
                    extraData: item
                })}
                style={[styles.dataChatItem, {
                    justifyContent: 'center', alignItems: 'center', borderTopLeftRadius: isFirstItem ? scale(20) : 0, borderTopRightRadius: isFirstItem ? scale(20) : 0, borderBottomRightRadius: isLastItem ? scale(20) : 0, borderBottomLeftRadius: isLastItem ? scale(20) : 0, borderBottomWidth: isLastItem ? 0 : scale(0.5), borderColor: GRAY_FONTCOLOR

                }]}>
                {
                    item?.read !== undefined && item?.read === false ?
                        <View style={{ position: 'absolute', width: scale(10), height: scale(10), borderRadius: scale(999), backgroundColor: '#4CB2F9', right: scale(10) }}></View>
                        : null
                }
                <View style={{ width: '3%', height: '100%', justifyContent: 'center', alignItems: 'flex-start' }}></View>
                <View style={{ width: '20%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                    {
                        item.image_path ?
                            <Image style={{
                                width: scale(80),
                                height: scale(80), borderRadius: scale(999)
                            }}
                                source={{
                                    uri: item?.image_path
                                }}
                            >
                            </Image> :
                            <Image
                                style={{
                                    width: scale(80),
                                    height: scale(80),
                                    borderRadius: scale(999)
                                }}
                                source={require('../../res/images/common/student_default_avatar.jpg')}>

                            </Image>
                    }
                    {
                        item?.active ?
                            <View style={{ backgroundColor: '#31a24c', width: scale(15), height: scale(15), borderRadius: scale(999), position: 'absolute', right: 5, bottom: 10 }}></View>
                            : null}
                </View>
                <View style={{ width: '2%', height: '100%', justifyContent: 'center', alignItems: 'flex-start' }}>

                </View>
                <View style={{ width: '76%', height: '100%', borderTopRightRadius: isFirstItem ? scale(20) : 0, borderBottomRightRadius: isLastItem ? scale(20) : 0, flexDirection: 'row' }}>
                    <View style={{ width: '65%', height: '100%', justifyContent: 'center', paddingLeft: scale(5) }}>
                        <Text allowFontScaling={false} style={{ fontSize: scale(16), fontWeight: 'bold', color: PRIMARY_COLOR }}>{item?.name}</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            {
                                item?.user_type_id === 1 ?
                                    <>
                                        <ImageIcon source={require('../../res/images/chat/parent.png')} size={scale(20)} />
                                        <Text numberOfLines={3} allowFontScaling={false} style={{ fontSize: scale(13), marginLeft: scale(5), color: GRAY_FONTCOLOR }}>{getString('COMMON_MESS_SCREEN_TEXT_PARENT_OF_STUDENT')}{item?.childName}</Text>
                                    </> :

                                    item?.user_type_id === 2 ?
                                        <>
                                            <ImageIcon source={require('../../res/images/chat/admin.png')} size={scale(20)} />
                                            <Text allowFontScaling={false} style={{ fontSize: scale(13), marginLeft: scale(5), color: GRAY_FONTCOLOR }}>{getString('COMMON_MESS_SCREEN_TEXT_ADMIN')}</Text>
                                        </> :
                                        item?.user_type_id === 5 ?
                                            <>
                                                <ImageIcon source={require('../../res/images/chat/monitor.png')} size={scale(20)} />
                                                <Text allowFontScaling={false} style={{ fontSize: scale(13), marginLeft: scale(5), color: GRAY_FONTCOLOR }}>{getString('COMMON_MESS_SCREEN_TEXT_TEACHER')}  {item?.class_name ? item?.class_name : ''}</Text>
                                            </>
                                            :
                                            <>
                                                <ImageIcon source={require('../../res/images/chat/monitor.png')} size={scale(20)} />
                                                <Text allowFontScaling={false} style={{ fontSize: scale(13), marginLeft: scale(5), color: GRAY_FONTCOLOR }}>{getString('COMMON_MESS_SCREEN_TEXT_MONITOR')}</Text>
                                            </>
                            }
                        </View>
                        {
                            item?.last_message ?
                                <>
                                    <Text allowFontScaling={false} numberOfLines={1} style={{ fontSize: scale(13), color: PRIMARY_COLOR, marginTop: scale(4) }}>{item?.from_me ? getString('COMON_MESS_SCREEN_TEXT_YOU') : ''}{item?.last_message}</Text>
                                </> :
                                null
                        }

                    </View>
                    <View style={{ width: '35%', height: '100%', justifyContent: 'flex-end', alignItems: 'flex-end', paddingRight: scale(10), paddingBottom: scale(8) }}>
                        {
                            item?.last_message ?
                                <Text allowFontScaling={false} numberOfLines={1} style={{ fontSize: scale(13), color: GRAY_FONTCOLOR }}>{moment(item?.created_at).format('DD/MM/YYYY')}</Text> : null
                        }
                    </View>
                </View>


            </TouchableOpacity>
        )
    }
    componentDidUpdate(prevProps) {
        if (this.props.requestUpdateContactScreenVar && !prevProps.requestUpdateContactScreenVar) {
            this._getChatUsers()
            this.props.requestUpdateContactScreen(false)
        }
    }
    _renderSearchedUsers(item) {
        console.log('user:', item)
        return (
            <TouchableOpacity
                onPress={() => this.props.navigation.navigate(INITCHAT, {
                    userId: item?.id,
                    name: item?.name,
                    extraData: item
                })}
                style={{ width: '95%', height: scale(50), marginTop: scale(5), flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                {
                    item?.image_path
                        ?
                        <FastImage
                            source={{ uri: item?.image_path }}
                            style={styles.image}>

                        </FastImage>
                        :
                        <FastImage
                            source={require('../../res/images/common/student_default_avatar.jpg')}
                            style={styles.image}>
                        </FastImage>
                }
                <View style={{ width: scale(2) }}></View>
                <View style={{ marginLeft: scale(0) }}>
                    <Text allowFontScaling={false} style={{ fontSize: scale(16), color: PRIMARY_COLOR, fontWeight: 'bold' }}>
                        {item?.name}
                    </Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        {
                            item?.user_type_id === 1 ?
                                <>
                                    <ImageIcon source={require('../../res/images/chat/parent.png')} size={scale(20)} />
                                    <Text allowFontScaling={false} style={{ fontSize: scale(13), marginLeft: scale(5), color: GRAY_FONTCOLOR }}>{getString('COMMON_MESS_SCREEN_TEXT_PARENT_OF_STUDENT')}{item?.childName || getString('UNKNOW')}</Text>
                                </> :

                                item?.user_type_id === 2 ?
                                    <>
                                        <ImageIcon source={require('../../res/images/chat/admin.png')} size={scale(20)} />
                                        <Text allowFontScaling={false} style={{ fontSize: scale(13), marginLeft: scale(5), color: GRAY_FONTCOLOR }}>{getString('COMMON_MESS_SCREEN_TEXT_ADMIN')}</Text>
                                    </> :
                                    item?.user_type_id === 5 ?
                                        <>
                                            <ImageIcon source={require('../../res/images/chat/monitor.png')} size={scale(20)} />
                                            <Text allowFontScaling={false} style={{ fontSize: scale(13), marginLeft: scale(5), color: GRAY_FONTCOLOR }}>{getString('COMMON_MESS_SCREEN_TEXT_TEACHER')} {item?.class_name || getString('UNKNOW')}</Text>
                                        </> :
                                        <>
                                            <ImageIcon source={require('../../res/images/chat/monitor.png')} size={scale(20)} />
                                            <Text allowFontScaling={false} style={{ fontSize: scale(13), marginLeft: scale(5), color: GRAY_FONTCOLOR }}>{getString('COMMON_MESS_SCREEN_TEXT_MONITOR')}</Text>
                                        </>
                        }
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
    _renderUsersAtContactScreen(item, index, type) {
        if (type === 'teacherToday') {
            console.log(item)
        }
        return (
            <TouchableOpacity
                onPress={() => this.props.navigation.navigate(INITCHAT, {
                    userId: item.id,
                    name: item.name,
                    extraData: item
                })}
                style={{ width: '100%', marginTop: scale(10), flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', paddingLeft: scale(5) }}>
                {
                    item?.image_path
                        ?
                        <View>
                            <FastImage
                                source={{ uri: item?.image_path }}
                                style={styles.image}>

                            </FastImage>
                            {
                                item?.active ?
                                    <View style={{ backgroundColor: '#31a24c', width: scale(10), height: scale(10), borderRadius: scale(999), position: 'absolute', right: 0, bottom: 0 }}></View>
                                    : null}
                        </View>
                        :
                        <View>
                            <FastImage
                                source={require('../../res/images/common/student_default_avatar.jpg')}
                                style={styles.image}>
                            </FastImage>
                            {
                                item?.active ?
                                    <View style={{ backgroundColor: '#31a24c', width: scale(10), height: scale(10), borderRadius: scale(999), position: 'absolute', right: 0, bottom: 0 }}></View>
                                    : null}
                        </View>
                }

                <View style={{ marginLeft: scale(10) }}>
                    <Text allowFontScaling={false} style={{ fontSize: scale(16), color: PRIMARY_COLOR, fontWeight: 'bold' }}>
                        {item?.name}
                    </Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        {
                            item?.childName ?
                                <Text allowFontScaling={false} style={{ fontSize: scale(13), color: GRAY_FONTCOLOR, fontWeight: '400' }}>
                                    {getString('COMMON_MESS_SCREEN_TEXT_PARENT_OF_STUDENT')}{item?.childName}
                                </Text>
                                : null
                        }
                        {
                            item?.class_name ?
                                <Text allowFontScaling={false} style={{ fontSize: scale(13), color: GRAY_FONTCOLOR, fontWeight: '400' }}>
                                    {getString('COMMON_MESS_SCREEN_TEXT_TEACHER')} {item?.class_name}
                                </Text>
                                : null
                        }
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
    _renderContactScreen() {
        return (
            <>
                <ScrollView
                    contentContainerStyle={{ justifyContent: 'center' }}
                    showsVerticalScrollIndicator={false}
                    style={{ width: '100%' }}
                >
                    {/* render admin */}
                    {
                        this.state.listContacts?.parentToday?.length !== 0 ?
                            <>
                                <Text allowFontScaling={false} style={styles.textTitle}>{getString('COMMON_MESS_SCREEN_TEXT_GUADIAN_TODAY')}</Text>
                                {
                                    this.state.listContacts?.parentToday.map((item, index) => {
                                        return this._renderUsersAtContactScreen(item, index, 'parentToday')
                                    })
                                }

                            </>
                            : null
                    }
                    {
                        this.state.listContacts?.teacherToday?.length !== 0 ?
                            <>
                                <Text allowFontScaling={false} style={styles.textTitle}>{getString('COMMON_MESS_SCREEN_TEXT_TEACHER_TODAY')}</Text>
                                {
                                    this.state.listContacts?.teacherToday.map((item, index) => {
                                        return this._renderUsersAtContactScreen(item, index, 'teacherToday')
                                    })
                                }

                            </>
                            : null
                    }
                    {
                        this.state.listContacts?.currentMonitor?.length !== 0 ?
                            <>
                                <Text allowFontScaling={false} style={styles.textTitle}>{getString('COMMON_MESS_SCREEN_TEXT_MONITOR_TODAY')}</Text>
                                {
                                    this.state.listContacts?.currentMonitor.map((item, index) => {
                                        return this._renderUsersAtContactScreen(item, index, 'currentMonitor')
                                    })
                                }

                            </>
                            : null
                    }

                    {
                        this.state.listContacts?.parentByClass?.length !== 0 ?
                            <>
                                <Text allowFontScaling={false} style={styles.textTitle}>{getString('COMMON_MESS_SCREEN_TEXT_LIST_GUADIAN')}</Text>
                                {
                                    this.state.listContacts?.parentByClass.map((item, index) => {
                                        return this._renderUsersAtContactScreen(item, index, 'parentByClass')
                                    })
                                }

                            </>
                            : null
                    }

                    {
                        this.state.listContacts?.teacherByStudent?.length !== 0 ?
                            <>
                                <Text allowFontScaling={false} style={styles.textTitle}>{getString('COMMON_MESS_SCREEN_TEXT_LIST_TEACHER')}</Text>
                                {
                                    this.state.listContacts?.teacherByStudent.map((item, index) => {
                                        return this._renderUsersAtContactScreen(item, index, 'teacherByStudent')
                                    })
                                }

                            </>
                            : null
                    }
                    {
                        this.state.listContacts?.monitors?.length !== 0 ?
                            <>
                                <Text allowFontScaling={false} style={styles.textTitle}>{getString('COMMON_MESS_SCREEN_TEXT_MONITOR')}:</Text>
                                {
                                    this.state.listContacts?.monitors.map((item, index) => {
                                        return this._renderUsersAtContactScreen(item, index, 'monitors')
                                    })
                                }

                            </>
                            : null
                    }
                    {
                        this.state.listContacts?.admin?.length !== 0 ?
                            <>
                                <Text allowFontScaling={false} style={styles.textTitle}>{getString('COMMON_MESS_SCREEN_TEXT_ADMIN')}:</Text>
                                {
                                    this.state.listContacts?.admin.map((item, index) => {
                                        return this._renderUsersAtContactScreen(item, index, 'admin')
                                    })
                                }

                            </>
                            : null
                    }
                    <View style={{ height: scale(150) }}></View>
                </ScrollView>
            </>
        )
    }

    render() {
        return (
            <View
                style={{ width: windowWidth, height: windowHeight, backgroundColor: BACKGROUND_COLOR, alignItems: 'center' }}>

                <Header
                    leftIcon={'arrow-left'}
                    onPressLeftIcon={() => {
                        socketInstance.getInstance().disconnect()
                        socketInstance.clearInstance()
                        this.props.navigation.pop()
                    }}
                    title={this.state.screenDisplay === 'chat' ? getString('COMMON_MESS_INIT_SCREEN_TEXT_HEADER') : getString('COMMON_MESS_SCREEN_TEXT_HEADER')}
                ></Header>
                {
                    this.state.screenDisplay === 'contact' ?
                        <>
                            <TextInput
                                placeholder={getString('TEXTINPUT_SEARCH')}
                                placeholderTextColor={GRAY_FONTCOLOR}
                                keyboardType={'default'}
                                onChangeText={async (value) => {
                                    await this.setState({
                                        dataSearch: value,
                                        callingSearchAPI: true
                                    })
                                    let params = {
                                        keyword: value
                                    }
                                    const response = await callApiWithParams('GET', SEARCH_CHAT, params)
                                    if (response && response?.status === 200 && response?.data?.code === 0) {
                                        this.setState({
                                            ...this.state,
                                            dataUserSearched: response?.data?.data,
                                            callingSearchAPI: false
                                        })
                                    }
                                }
                                }
                                style={{
                                    paddingLeft: scale(8),
                                    color: BLACK,
                                    width: '95%',
                                    backgroundColor: WHITE,
                                    height: scale(35),
                                    marginTop: scale(10),
                                    borderRadius: scale(20),
                                    borderColor: GRAY_LIGHT,
                                    borderWidth: scale(0.5)
                                }}></TextInput>
                            {
                                this.state.dataSearch !== '' ?
                                    <ScrollView
                                        contentContainerStyle={{ justifyContent: 'center', alignItems: 'center', }}
                                        showsVerticalScrollIndicator={false}
                                        style={{ width: '95%', borderRadius: scale(20) }}
                                    >
                                        {
                                            this.state.callingSearchAPI ?
                                                <Text allowFontScaling={false} style={{ marginTop: scale(10), color: GRAY_FONTCOLOR, fontWeight: 'bold', fontSize: scale(13) }}>{getString('COMMON_MESS_SCREEN_TEXT_SEARCHING')}</Text> : null}
                                        {
                                            this.state?.dataUserSearched.map((item, index) => {
                                                return this._renderSearchedUsers(item)
                                            })
                                        }
                                        <View style={{ height: scale(50) }}></View>
                                    </ScrollView>
                                    :
                                    this._renderContactScreen()

                            }
                        </>
                        :
                        <ScrollView
                            contentContainerStyle={{ justifyContent: 'center', alignItems: 'center', }}
                            showsVerticalScrollIndicator={false}
                            style={{ width: '95%', borderRadius: scale(20) }}
                        >
                            <View style={{ height: scale(10), width: '100%', justifyContent: 'center', alignItems: 'flex-start' }}>

                            </View>

                            {
                                this.state?.users.map((item, index) => {
                                    return this._renderUsers(item, index, this.state?.users.length - 1)
                                })
                            }

                            <View style={{ height: scale(20) + headerHeight }}></View>
                        </ScrollView>

                }
                <View style={{ height: scale(15) }}></View>
                {/* bottom Tab */}
                <View
                    activeOpacity={1}
                    style={{ position: 'absolute', width: windowWidth, height: isLargePhone ? headerHeight + scale(10) : headerHeight, bottom: 0, flexDirection: 'row', backgroundColor: WHITE, justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({
                                ...this.state,
                                screenDisplay: 'chat'
                            })
                        }}
                        activeOpacity={1}
                        style={{ width: '50%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                        <ImageIcon
                            source={this.state.screenDisplay === 'chat' ? require('../../res/common_image/chat/chat.png') : require('../../res/common_image/chat/chatUncheck.png')}
                        ></ImageIcon>
                    </TouchableOpacity>
                    <View style={{ height: '50%', width: scale(1), backgroundColor: PRIMARY_COLOR }}></View>
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({
                                ...this.state,
                                screenDisplay: 'contact'
                            })
                        }}
                        activeOpacity={1}
                        style={{ width: '50%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                        <ImageIcon
                            size={this.state.screenDisplay === 'contact' ? scale(30) : scale(28)}
                            source={this.state.screenDisplay === 'contact' ? require('../../res/common_image/chat/contacts.png') : require('../../res/common_image/chat/contactsUncheck.png')}
                        ></ImageIcon>
                    </TouchableOpacity>

                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: GRAY_LIGHT


    },
    input: {
        justifyContent: 'center',
        fontSize: scale(13),
        textAlign: 'left',
        height: scale(33),
        fontSize: scale(13),
        paddingVertical: scale(-5),
        backgroundColor: WHITE,
        borderRadius: scale(15),
        flex: 8.5
    },
    searchContainer: {
        flexDirection: 'row',
        width: '85%',
        justifyContent: 'center',
        alignItems: 'center',
        height: scale(35),
        backgroundColor: WHITE,
        borderWidth: scale(0.5),
        borderRadius: scale(15),
        marginBottom: scale(10)
    },
    dataChat: {
        alignItems: 'center',
        justifyContent: 'center',


    },
    dataChatItem: {
        width: '100%',
        height: scale(50),
        flexDirection: 'row',
        backgroundColor: WHITE,
        height: scale(90),
        borderBottomWidth: scale(0.5)
    },
    image: {
        width: scale(45),
        height: scale(45),
        backgroundColor: WHITE,
        borderRadius: scale(999)
    },
    textTitle: {
        color: GRAY_FONTCOLOR,
        fontSize: scale(16),
        fontWeight: 'bold',
        marginTop: scale(10),
        marginLeft: scale(5)
    }
})
const mapStateToProps = (store) => {
    return {
        requestUpdateContactScreenVar: store.systemReducer.requestUpdateContactScreen
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        requestUpdateContactScreen: (payload) => {
            dispatch(requestUpdateContactScreen(payload))
        },
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(ChatScreen);
